import React, { useEffect, useState } from 'react'
import { Table } from '@contentful/f36-table'
import { Modal } from '@contentful/f36-modal'
import { Select } from '@contentful/f36-forms'
import { fetchItemCollection } from './services/fetchItemCollection'
import { type ContentTypeVersionItem } from './types'
import { fetchItem } from './services/fetchItem'

export const App = () => {
    const [itemCollection, setItemCollection] = useState<ContentTypeVersionItem[]>([])
    const [isViewTypeModalOpen, setIsViewTypeModalOpen] = useState(false)
    const [viewItem, setViewItem] = useState<ContentTypeVersionItem | undefined>(undefined)
    const [viewItemVersion, setViewItemVersion] = useState<string | undefined>()

    useEffect(() => {
        fetchItemCollection().then((data) => setItemCollection(data.Items))
    }, [])

    const handleItemClick = (id: string) => {
        fetchItem(id).then((data) => {
            console.log(data)
            setViewItem(data)
            setIsViewTypeModalOpen(true)
        })
    }

    const handleViewVersionChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
        console.log(e.target.value)
        setViewItemVersion(e.target.value)
    }

    return (
        <>
            <Modal onClose={() => setIsViewTypeModalOpen(false)} isShown={isViewTypeModalOpen}>
                {() => {
                    const versions = Object.keys(viewItem?.versions || []).sort()
                    const latestVersion = versions.slice(-1)[0]
                    return <>
                        <Modal.Header
                            // eslint-disable-next-line @typescript-eslint/no-explicit-any
                            title={viewItem?.id as any || 'Loading...'}
                            onClose={() => setIsViewTypeModalOpen(false)}
                        >
                            <Select
                                id="optionSelect-controlled"
                                name="optionSelect-controlled"
                                value={viewItemVersion || latestVersion}
                                onChange={handleViewVersionChange}
                            >
                                {versions.map((version) => (
                                    <Select.Option
                                        key={`version-option-${version}`}
                                        value={version}
                                    >
                                        {version}
                                    </Select.Option>
                                ))}

                            </Select>
                        </Modal.Header>
                        <Modal.Content>
                            {viewItemVersion && JSON.stringify(viewItem?.versions[viewItemVersion])}
                        </Modal.Content>
                    </>
                }}
            </Modal>
            <Table>
                <Table.Head>
                    <Table.Row>
                        <Table.Cell>Content Type ID</Table.Cell>
                        <Table.Cell>Latest Version</Table.Cell>
                    </Table.Row>
                </Table.Head>
                <Table.Body>
                    {
                        itemCollection?.map((item) => {
                            const latestVersion = Object.keys(item.versions.M).sort().pop()
                            const id = item.id.S
                            return (
                                <Table.Row
                                    key={id}
                                    id={`collection-item-${id}`}
                                    onClick={() => handleItemClick(id)}
                                >
                                    <Table.Cell>
                                        {id}
                                    </Table.Cell>
                                    <Table.Cell>
                                        {latestVersion}
                                    </Table.Cell>
                                </Table.Row>
                            )
                        })
                    }
                </Table.Body>
            </Table>
        </>
    )
}