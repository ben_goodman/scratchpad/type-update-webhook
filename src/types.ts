import { ContentTypeJSON } from '../.aws/terraform/api_handler/src/types'

export interface DynamoString {
    S: string
}

export interface DynamoNumber {
    N: number
}

export interface DynamoList<T> {
    L: Array<T>
}

export interface DynamoScanResponse<T> {
    $metadata: {
        httpStatusCode: number,
        requestId: string,
        extendedRequestId: string|undefined,
        cfId: string|undefined,
        attempts: number,
        totalRetryDelay: number,
    }
    Items: Array<T>
    Count: number
    ScannedCount: number
    LastEvaluatedKey?: {id: DynamoString}
}

export interface ContentTypeVersionItem {
    id: DynamoString
    deleted: boolean
    versions: {
        [version: string]: ContentTypeJSON
    }
}
