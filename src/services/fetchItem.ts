import { ContentTypeVersionItem } from 'src/types'

export const fetchItem = async (typeId: string): Promise<ContentTypeVersionItem> => {
    const resp = await fetch(`${process.env.API_ENDPOINT}/${typeId}`)
    return resp.json()
}