import type { ContentTypeVersionItem, DynamoScanResponse } from './../types'

export const fetchItemCollection = async (): Promise<DynamoScanResponse<ContentTypeVersionItem>> => {
    const resp = await fetch(`${process.env.API_ENDPOINT}/`)
    return resp.json()
}