export type CTFSys = {
    type: string,
    linkType: string,
    id: string
}

export interface CTFField {
    id: string,
    name: string,
    type: string,
    localized: boolean,
    required: boolean,
    validations: any[],
    disabled: boolean,
    omitted: boolean,
}

export interface ContentTypeJSON {
    sys: {
        space: {
            sys: CTFSys
        },
        id: string,
        type: string,
        createdAt: string,
        updatedAt: string,
        environment: {
            sys: CTFSys
        },
        publishedVersion: string,
        publishedAt: string,
        firstPublishedAt: string,
        createdBy: {
            sys: CTFSys
        },
        updatedBy: {
            sys: CTFSys
        },
        publishedCounter: number,
        version ?: number,
        revision ?: number,
        publishedBy: {
            sys: CTFSys
        },
        urn: string
    },
    displayField: string
    name: string,
    description: string,
    fields: CTFField[]
}