import type { APIGatewayProxyEventV2, APIGatewayProxyResultV2 } from 'aws-lambda'
import queryString from 'query-string';
import { getItem } from './getItem'
import { scanItems } from './scanItems';

export const handler = async (event: APIGatewayProxyEventV2): Promise<APIGatewayProxyResultV2> => {
    const method = event.requestContext.http.method
    const paths = event.rawPath.split('/').filter(x => x !== '')
    const {
        limit = undefined,
        from = undefined,
    } = queryString.parse(event.rawQueryString, {parseNumbers: true})


    console.log('event', event)

    // get a paged scan of all items
    // GET /?limit={number=10}&from={number=0}
    if (method === 'GET' && !paths[0] ) {
        return scanItems({
            limit: limit as number|undefined,
            from: from as string|undefined
        })
    }

    // test the api
    // GET /health
    if (method === 'GET' && paths[0] === 'health') {
        return {
            headers: {"Content-Type": "application/json"},
            statusCode: 200,
            body: JSON.stringify({ message: 'Ok' }),
        }
    }

    // get a specific type
    // GET /{type_id}
    if (method === 'GET' && paths[0]) {
        const contentTypeItem = await getItem(paths[0])
        return {
            headers: {"Content-Type": "application/json"},
            statusCode: 200,
            body: JSON.stringify(contentTypeItem),
        }
    }

    return {
        headers: {"Content-Type": "application/json"},
        statusCode: 400,
        body: JSON.stringify({ message: 'Unsupported Route' }),
    }

}
