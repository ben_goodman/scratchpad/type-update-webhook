import { DynamoDBDocumentClient, GetCommand } from '@aws-sdk/lib-dynamodb';
import { DynamoDBClient } from '@aws-sdk/client-dynamodb';
import { ContentTypeJSON } from './types';

interface ContentTypeVersionItem {
    id: string,
    deleted: boolean,
    versions: {
        [version: string]: ContentTypeJSON
    }
}

const client = new DynamoDBClient({})
const dynamo = DynamoDBDocumentClient.from(client)
const TABLE_NAME = process.env.TABLE_NAME

export const getItem = async (id: string): Promise<ContentTypeVersionItem | undefined> => {
    const resp = await dynamo.send(
        new GetCommand({
            TableName: TABLE_NAME,
            Key: {
                id,
            },
        })
    )

    return resp.Item as ContentTypeVersionItem|undefined
}