import { DynamoDBClient } from '@aws-sdk/client-dynamodb'
import { DynamoDBDocumentClient, GetCommand, PutCommand } from '@aws-sdk/lib-dynamodb'
import { ContentTypeJSON } from './types'

const client = new DynamoDBClient({})
const dynamo = DynamoDBDocumentClient.from(client)
const TABLE_NAME = process.env.TABLE_NAME

interface ContentTypeItem {
    name: string
    versions: {
        [version: string]: ContentTypeJSON
    }
}

const getItem = async (id: string): Promise<ContentTypeItem|undefined> => {
    const response = await dynamo.send(
        new GetCommand({
            TableName: TABLE_NAME,
            Key: {
                id,
            },
        })
    );

    return response.Item as ContentTypeItem|undefined
}


const setItemAsDeleted = async (webhookPayload: ContentTypeJSON): Promise<void> => {
    const id = webhookPayload.sys.id
    const thisVersionNumber = webhookPayload.sys.version

    console.log(`Upserting type ${id} version ${thisVersionNumber}`)

    if (id === undefined || thisVersionNumber === undefined) {
        throw new Error('Missing id or version number')
    }

    const existingItem = await getItem(id)

    await dynamo.send(
        new PutCommand({
            TableName: TABLE_NAME,
            Item: {
                ...existingItem,
                deleted: true,
            },
        })
    ).catch((err) => {
        console.log(err)
        throw err
    })
}

export const deleteVersion = async (body?: string|undefined): Promise<void> => {
    if (body === undefined) {
        throw new Error('No body provided')
    }

    const webhookPayload = JSON.parse(body)

    return setItemAsDeleted(webhookPayload)
}
