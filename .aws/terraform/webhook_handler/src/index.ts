import type { APIGatewayProxyEventV2, APIGatewayProxyResultV2 } from 'aws-lambda'
import { addVersion } from './addVersion'
import { deleteVersion } from './deleteVersion'

export const handler = async (event: APIGatewayProxyEventV2): Promise<APIGatewayProxyResultV2> => {
    const method = event.requestContext.http.method
    const paths = event.rawPath.split('/').filter(x => x !== '')
    const headers = event.headers

    const body = event.body && event.isBase64Encoded ? Buffer.from(event.body!, 'base64').toString('utf-8') : event.body

    // test the api
    // GET /health
    if (method === 'GET' && paths[0] === 'health') {
        return {
            headers: {"Content-Type": "application/json"},
            statusCode: 200,
            body: JSON.stringify({ message: 'Ok' }),
        }
    }

    // default response - serve webhook
    console.log('event', body)
    console.log('headers', headers)

    try {
        if ( headers['x-contentful-topic'] === 'ContentManagement.ContentType.save') {
            const version = await addVersion(body)
            const message = `Processed webhook topic: ${headers['x-contentful-topic']}. For version: ${version}.`
            return {
                headers: {"Content-Type": "application/json"},
                statusCode: 200,
                body: JSON.stringify({ message }),
            }
        } else if ( headers['x-contentful-topic'] === 'ContentManagement.ContentType.delete') {
            await deleteVersion(body)
            const message = `Processed webhook topic: ${headers['x-contentful-topic']}`
            return {
                headers: {"Content-Type": "application/json"},
                statusCode: 200,
                body: JSON.stringify({ message }),
            }
        } else {
            const message = `Unsupported webhook topic: ${headers['x-contentful-topic']}`
            return {
                headers: {"Content-Type": "application/json"},
                statusCode: 400,
                body: JSON.stringify({ message }),
            }
        }
    } catch (err) {
        console.error(err)
        return {
            headers: {"Content-Type": "application/json"},
            statusCode: 500,
            body: JSON.stringify({ message: 'Internal Server Error' }),
        }
    }
}
