import { DynamoDBClient } from '@aws-sdk/client-dynamodb'
import { DynamoDBDocumentClient, GetCommand, PutCommand } from '@aws-sdk/lib-dynamodb'
import { ContentTypeJSON } from './types'

const client = new DynamoDBClient({})
const dynamo = DynamoDBDocumentClient.from(client)
const TABLE_NAME = process.env.TABLE_NAME

interface ContentTypeItem {
    name: string
    versions: {
        [version: string]: ContentTypeJSON
    }
}

const sortByKey = (obj: {}) => {
    const sortedKeys = Object.keys(obj).sort()
    return Object.fromEntries(sortedKeys.map(key => [key, obj[key]]))
}

const getItem = async (id: string): Promise<ContentTypeItem|undefined> => {
    const response = await dynamo.send(
        new GetCommand({
            TableName: TABLE_NAME,
            Key: {
                id,
            },
        })
    );

    return response.Item as ContentTypeItem|undefined
}

// updates an item if it exists, otherwise creates a new one.
// DDB's `put` command will _overwrite_ the item if it already exists,
// so additional steps are taken.
const upsertItem = async (webhookPayload: ContentTypeJSON): Promise<string> => {
    const id = webhookPayload.sys.id
    const thisVersionNumber = webhookPayload.sys.version

    console.log(`Upserting type ${id} version ${thisVersionNumber}`)

    if (id === undefined || thisVersionNumber === undefined) {
        throw new Error('Missing id or version number')
    }

    // only retain the last 10 versions
    const existingItem = await getItem(id)
    const sortedPriorVersions = sortByKey(existingItem?.versions || {})
    const truncatedPriorVersions = Object.fromEntries(Object.entries(sortedPriorVersions).slice(-10))

    const versions = {
        ...truncatedPriorVersions,
        [thisVersionNumber]: webhookPayload,
    }

    await dynamo.send(
        new PutCommand({
            TableName: TABLE_NAME,
            Item: {
                id,
                versions,
                deleted: false,
            },
        })
    ).catch((err) => {
        console.log(err)
        throw err
    })

    return thisVersionNumber
}

export const addVersion = async (body?: string|undefined): Promise<string> => {
    if (body === undefined) {
        throw new Error('No body provided')
    }

    const webhookPayload = JSON.parse(body)

    return upsertItem(webhookPayload)
}
