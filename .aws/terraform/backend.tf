terraform {
    backend "s3" {
        bucket = "tf-states-us-east-1-bgoodman"
        key    = "ctf-type-update-webhook/terraform.tfstate"
        region = "us-east-1"
    }
}