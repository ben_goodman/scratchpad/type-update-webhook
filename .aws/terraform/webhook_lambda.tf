module "webhook_handler_payload" {
    source  = "gitlab.com/ben_goodman/lambda-function/aws//modules/code_builder"
    version = "3.0.0-52140be3-rc"

    output_filename = "index.js"
    source_root = "${path.module}/webhook_handler"
    build_command = "make all"
    environment_variables = {
        TABLE_NAME  = aws_dynamodb_table.type_versions_database.name
    }
}

module "webhook_lambda_function" {
    source  = "gitlab.com/ben_goodman/lambda-function/aws"
    version = "3.0.0-52140be3-rc"

    org              = var.resource_namespace
    project_name     = var.project_name
    lambda_payload   = module.webhook_handler_payload.archive_file
    function_name    = "webhook-handler-${random_id.cd_function_suffix.hex}"
    function_handler = "index.handler"
    publish          = true
    memory_size      = 512
    role             = aws_iam_role.webhook_lambda_role
}

resource "aws_lambda_function_url" "webhook_lambda_function_url" {
    function_name      = module.webhook_lambda_function.name
    authorization_type = "NONE"
}
