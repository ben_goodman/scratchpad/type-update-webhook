output "webhook_url" {
    value = aws_lambda_function_url.webhook_lambda_function_url.function_url
}

output "api_url" {
    value = aws_lambda_function_url.api_lambda_function_url.function_url
}