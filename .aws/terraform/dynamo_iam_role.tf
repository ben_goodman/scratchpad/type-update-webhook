resource "aws_iam_policy" "dynamo_db_access_policy" {
  name = "dynamo-db-access-policy-${random_id.cd_function_suffix.hex}"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Action = [
            "dynamodb:Scan",
            "dynamodb:GetRecords",
            "dynamodb:GetItem",
            "dynamodb:PutItem",
            "dynamodb:UpdateItem",
            "dynamodb:DeleteItem",
            "dynamodb:GetShardIterator",
            "dynamodb:DescribeStream",
            "dynamodb:ListStreams"
        ]
        Resource = [
            aws_dynamodb_table.type_versions_database.arn
        ]
      }
    ]
  })
}


resource "aws_iam_role" "webhook_lambda_role" {
  name = "ctf-webhook-lambda-exec-role-${random_id.cd_function_suffix.hex}"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Principal = {
          Service = "lambda.amazonaws.com"
        }
        Action = "sts:AssumeRole"
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "ddb_lambda_role_attachment" {
    role       = aws_iam_role.webhook_lambda_role.name
    policy_arn = aws_iam_policy.dynamo_db_access_policy.arn
}